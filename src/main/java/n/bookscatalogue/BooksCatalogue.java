package n.bookscatalogue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

class Book implements Serializable {
    int id;
    String title;
    String author;
    float cost;

    public Book(int id, String title, String author, float cost) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "ID: " + id + "\nTitle: " + title + "\nAuthor: " + author + "\nCost: " + cost;
    }
}

class EBook extends Book {
    String fileFormat;
    
    public EBook(int id, String title, String author, float cost, String fileFormat) {
        super(id, title, author, cost);
        this.fileFormat = fileFormat;
    }
    
    @Override
    public String toString() {
        return super.toString() + "\nFile Format: " + fileFormat + "\n";
    }
}

class AudioBook extends Book {
    String narrator;
    
    public AudioBook(int id, String title, String author, float cost, String narrator) {
        super(id, title, author, cost);
        this.narrator = narrator;
    }
    
    @Override
    public String toString() {
        return super.toString() + "\nNarrator: " + narrator + "\n";
    }
}

public class BooksCatalogue extends JFrame {
    JComboBox<String> subcategoryComboBox;
    JTextField idField;
    JTextField titleField;
    JTextField authorField;
    JTextField costField;
    JTextField extraSubCategoryField;
    JButton addButton;
    JButton saveButton;
    JTextArea displayArea;
    JTextField searchField;
    JButton searchButton;
    JButton clearSearchButton;
    JButton mostExpensiveBookButton;
    JLabel extraLabel = new JLabel("File Format: ", SwingConstants.RIGHT);

    private ArrayList<Book> bookList = new ArrayList<>();

    public BooksCatalogue() {
        setTitle("Book Catalogue");
        setSize(400, 650);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        String[] subcategories = {"eBook", "Audio Book"};
        subcategoryComboBox = new JComboBox<>(subcategories);
        subcategoryComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (subcategoryComboBox.getSelectedItem() == "eBook") {
                    extraLabel.setText("File Format: ");
                } else {
                    extraLabel.setText("Narrator: ");
                }
                clearItems();
            }
        });

        idField = new JTextField(20);
        titleField = new JTextField(20);
        authorField = new JTextField(20);
        costField = new JTextField(20);
        extraSubCategoryField = new JTextField(20);
        
        BooksCatalogue cat = this;
        addButton = new JButton("Add Book");
        addButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                int id;
                if (!titleField.getText().equals("") &&
                        !authorField.getText().equals("") &&
                        !extraSubCategoryField.getText().equals("")){
                    
                    try{
                    id = Integer.parseInt(idField.getText());
                    String title = titleField.getText();
                    String author = authorField.getText();
                    float cost = Float.parseFloat(costField.getText());
                    
                    String subcategory = (String) subcategoryComboBox.getSelectedItem();
                    String extraSubCategory = extraSubCategoryField.getText();

                    if (subcategory.equals("eBook")) {
                        EBook textBook = new EBook(id, title, author, cost, extraSubCategory);
                        bookList.add(textBook);
                    } else if (subcategory.equals("Audio Book")) {
                        AudioBook audioBook = new AudioBook(id, title, author, cost, extraSubCategory);
                        bookList.add(audioBook);
                    }
                    
                    clearItems();
                    displayBooks();
                    }
                    catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(cat, "Please enter numeric values for Book ID and Cost");
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(cat, "Please fill the in missing values");
                }
                
            }
        });

        saveButton = new JButton("Save Books to File");
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToFile();
                clearItems();
            }
        });

        searchField = new JTextField(20);
        searchButton = new JButton("Search");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String searchTerm = searchField.getText();
                searchBooksByTitle(searchTerm);
            }
        });

        displayArea = new JTextArea(21, 30);
        displayArea.setEditable(false);
        
        clearSearchButton = new JButton("Clear");
        clearSearchButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               clearItems();
           }
        });
        
        mostExpensiveBookButton = new JButton("Get Most Expensive Book");
        mostExpensiveBookButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               clearItems();
               getMostExpensiveBook();
           }
        });
       
        
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(0, 2));
        inputPanel.add(new JLabel("Subcategory:", SwingConstants.RIGHT));
        inputPanel.add(subcategoryComboBox);
        inputPanel.add(new JLabel("Book ID:", SwingConstants.RIGHT));
        inputPanel.add(idField);
        inputPanel.add(new JLabel("Title:", SwingConstants.RIGHT));
        inputPanel.add(titleField);
        inputPanel.add(new JLabel("Author:", SwingConstants.RIGHT));
        inputPanel.add(authorField);
        inputPanel.add(new JLabel("Cost:", SwingConstants.RIGHT));
        inputPanel.add(costField);
        inputPanel.add(extraLabel);
        inputPanel.add(extraSubCategoryField);
        inputPanel.add(new JLabel(""));
        inputPanel.add(addButton);
        inputPanel.add(new JLabel(""));
        inputPanel.add(saveButton);

        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new FlowLayout());
        searchPanel.add(new JLabel("Search by Title:"));
        searchPanel.add(searchField);
        searchPanel.add(searchButton);
        searchPanel.add(clearSearchButton);
        searchPanel.add(mostExpensiveBookButton);

        JScrollPane scrollPane = new JScrollPane(displayArea);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(inputPanel, BorderLayout.NORTH);
        getContentPane().add(searchPanel, BorderLayout.CENTER);
        getContentPane().add(scrollPane, BorderLayout.SOUTH);
    }

    private void displayBooks() {
        displayArea.setText("");
        for (Book book : bookList) {
            displayArea.append(book.toString() + "\n");
        }
    }
    
    private void clearItems () {
        displayBooks();
               searchField.setText("");
               idField.setText("");
               titleField.setText("");
               authorField.setText("");
               costField.setText("");
               extraSubCategoryField.setText("");
    }

    private void saveToFile() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("books.dat"))) {
            outputStream.writeObject(bookList);
            JOptionPane.showMessageDialog(this, "Books saved to file successfully.");
            clearItems();
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error while saving books to file.");
        }
    }

    private void loadFromFile() {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("books.dat"))) {
            bookList = (ArrayList<Book>) inputStream.readObject();
            displayBooks();
            JOptionPane.showMessageDialog(this, "Books loaded from file successfully.");
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Error while loading books from file.");
        }
    }

    private void searchBooksByTitle(String searchTerm) {
        displayArea.setText("");
        for (Book book : bookList) {
            if (book.title.toLowerCase().contains(searchTerm.toLowerCase())) {
                displayArea.append(book.toString() + "\n");
            }
        }
    }
    
    private void getMostExpensiveBook() {
        displayArea.setText("");
        Book tempBook = bookList.get(0);
        float lowestPrice = 0.0f;
        for (Book book : bookList) {
            if (book.cost > lowestPrice) {
                lowestPrice = book.cost;
                tempBook = book;
            }
        }
        displayArea.append(tempBook.toString() + "\n");
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                BooksCatalogue bookApp = new BooksCatalogue();
                bookApp.loadFromFile(); // Load saved books
                bookApp.setVisible(true);
            }
        });
    }
}
