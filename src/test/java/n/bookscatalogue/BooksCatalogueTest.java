package n.bookscatalogue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BooksCatalogueTest {
    
    private BooksCatalogue bookApp;

    @BeforeEach
    public void setUp() {
        bookApp = new BooksCatalogue();
    }

    @Test
    public void testAddEBook() {
        bookApp.subcategoryComboBox.setSelectedItem("eBook");
        bookApp.idField.setText("1");
        bookApp.titleField.setText("EBook Title");
        bookApp.authorField.setText("EBook Author");
        bookApp.costField.setText("10.99");
        bookApp.extraSubCategoryField.setText("PDF");
        bookApp.addButton.doClick();
        
        String displayText = bookApp.displayArea.getText();
        assertTrue(displayText.contains("1"));
        assertTrue(displayText.contains("EBook Title"));
        assertTrue(displayText.contains("EBook Author"));
        assertTrue(displayText.contains("10.99"));
        assertTrue(displayText.contains("File Format: PDF"));
    }
    
    @Test
    public void testAddAudioBook() {
        bookApp.subcategoryComboBox.setSelectedItem("Audio Book");
        bookApp.idField.setText("2");
        bookApp.titleField.setText("Audio Book Title");
        bookApp.authorField.setText("Audio Book Author");
        bookApp.costField.setText("15.99");
        bookApp.extraSubCategoryField.setText("Narrator Name");
        bookApp.addButton.doClick();
        
        String displayText = bookApp.displayArea.getText();
        assertTrue(displayText.contains("2"));
        assertTrue(displayText.contains("Audio Book Title"));
        assertTrue(displayText.contains("Audio Book Author"));
        assertTrue(displayText.contains("15.99"));
        assertTrue(displayText.contains("Narrator: Narrator Name"));
    }
    
    @Test
    public void testMostExpensiveBook() {
        bookApp.subcategoryComboBox.setSelectedItem("eBook");
        bookApp.idField.setText("1");
        bookApp.titleField.setText("EBook Title");
        bookApp.authorField.setText("EBook Author");
        bookApp.costField.setText("10.99");
        bookApp.extraSubCategoryField.setText("PDF");
        bookApp.addButton.doClick();
        
        bookApp.subcategoryComboBox.setSelectedItem("Audio Book");
        bookApp.idField.setText("2");
        bookApp.titleField.setText("Audio Book 2");
        bookApp.authorField.setText("Author 5");
        bookApp.costField.setText("75.99");
        bookApp.extraSubCategoryField.setText("Stephen Fry");
        bookApp.addButton.doClick();
        
        bookApp.subcategoryComboBox.setSelectedItem("Audio Book");
        bookApp.idField.setText("3");
        bookApp.titleField.setText("Audio Book Title 3");
        bookApp.authorField.setText("Audio Book Author");
        bookApp.costField.setText("15.99");
        bookApp.extraSubCategoryField.setText("Narrator Name");
        bookApp.addButton.doClick();
        
        String displayText = bookApp.displayArea.getText();
        assertTrue(displayText.contains("2"));
        assertTrue(displayText.contains("Audio Book 2"));
        assertTrue(displayText.contains("Author 5"));
        assertTrue(displayText.contains("75.99"));
        assertTrue(displayText.contains("Narrator: Stephen Fry"));
    }
    
    @Test
    public void testSearchByTitle() {
        bookApp.subcategoryComboBox.setSelectedItem("eBook");
        bookApp.idField.setText("1");
        bookApp.titleField.setText("War");
        bookApp.authorField.setText("James Gibson");
        bookApp.costField.setText("7.99");
        bookApp.extraSubCategoryField.setText("PDF");
        bookApp.addButton.doClick();
        
        bookApp.subcategoryComboBox.setSelectedItem("Audio Book");
        bookApp.idField.setText("2");
        bookApp.titleField.setText("Harry Potter and the Prisoner of Azkaban");
        bookApp.authorField.setText("J. K. Rowling");
        bookApp.costField.setText("23.55");
        bookApp.extraSubCategoryField.setText("Stephen Fry");
        bookApp.addButton.doClick();
        
        bookApp.subcategoryComboBox.setSelectedItem("eBook");
        bookApp.idField.setText("3");
        bookApp.titleField.setText("Killing Floor");
        bookApp.authorField.setText("Lee Child");
        bookApp.costField.setText("25.99");
        bookApp.extraSubCategoryField.setText("Scott Brick");
        bookApp.addButton.doClick();
        
        bookApp.searchField.setText("Potter");
        bookApp.searchButton.doClick();
        
        String displayText = bookApp.displayArea.getText();
        assertTrue(displayText.contains("2"));
        assertTrue(displayText.contains("Harry Potter and the Prisoner of Azkaban"));
        assertTrue(displayText.contains("J. K. Rowling"));
        assertTrue(displayText.contains("23.55"));
        assertTrue(displayText.contains("Narrator: Stephen Fry"));
        
        assertFalse(displayText.contains("Lee Child"));  
    }
    
    @Test
    public void testClearButton() {
        bookApp.subcategoryComboBox.setSelectedItem("eBook");
        bookApp.idField.setText("1");
        bookApp.titleField.setText("War");
        bookApp.authorField.setText("James Gibson");
        
        bookApp.clearSearchButton.doClick();
        
        assertTrue(bookApp.idField.getText().equals(""));
        assertTrue(bookApp.titleField.getText().equals(""));
        assertTrue(bookApp.authorField.getText().equals(""));
    }  
}
